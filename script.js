function addProduct() {
    const productName = document.getElementById('productName').value;
    const productQuantity = document.getElementById('productQuantity').value;

    if (productName === '' || productQuantity === '') {
        alert('Пожалуйста, заполните все поля!');
        return;
    }

    const productList = document.getElementById('product-list');
    const productItem = document.createElement('div');
    productItem.classList.add('product-item');

    productItem.innerHTML = `
        <span class="product-name">${productName}</span>
        <span> - Количество: ${productQuantity}</span>
        <button onclick="deleteProduct(this)">Удалить</button>
        <button onclick="editProduct(this, '${productName}', '${productQuantity}')">Редактировать</button>
    `;

    productList.appendChild(productItem);

    document.getElementById('productName').value = '';
    document.getElementById('productQuantity').value = '';
}

function deleteProduct(button) {
    button.parentElement.remove();
}

function editProduct(button, name, quantity) {
    document.getElementById('productName').value = name;
    document.getElementById('productQuantity').value = quantity;
    deleteProduct(button);
}